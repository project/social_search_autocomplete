# Social Search Autocomplete


## Table of contents

- Introduction
- Requirements
- Installation
- Maintainers

## Introduction

- This is mitigated by using a SOLR backend.

## Requirements

This module requires the following modules:

- [update_helper](https://www.drupal.org/project/update_helper)

## Installation

- Install the Favicon module as you would normally install
  a contributed Drupal module.
  [Visit](https://www.drupal.org/node/1897420) for further information.


## Maintainers

- Tiago Siqueira - [tbsiqueira](https://www.drupal.org/u/tbsiqueira)
- Nishant Kumar  - [Nishant](https://www.drupal.org/u/nishant)
- Alexander Varwijk - [kingdutch](https://www.drupal.org/u/kingdutch)
- Navneet Singh  - [navneet0693](https://www.drupal.org/u/navneet0693)
- open-social - [maheshkp92](https://www.drupal.org/u/open-social)
- Ronald te Brake  - [ronaldtebrake](https://www.drupal.org/u/ronaldtebrake)
- Nishant Kumar  - [nishant](https://www.drupal.org/u/nishant)
